require 'dnsbl_checker/config'
require 'dnsbl_checker/result_ip'

module DnsblChecker
  class Checker
    attr_reader :ips, :blacklists

    def self.call(ips)
      new(ips).perform
    end

    def initialize(ips)
      @ips = [*ips]
      @blacklists = list_blacklists
    end

    def perform
      ips.inject({}) { |acc, elem| acc.merge(elem => result_ip(elem)) }
    end

    private

    def result_ip(ip)
      ResultIp.new(ip, blacklists).perform!
    end

    def list_blacklists
      DnsblChecker::Config.blacklists_file ? blacklists_from_file : DnsblChecker::Config.blacklists_array
    end

    def blacklists_from_file
      file = File.read(blacklists_file)
      file.split('\/n')
    end
  end
end
