require 'dnsbl_checker/ip_checker'
require 'virtus'

module DnsblChecker
  class ResultIp
    IP_REGEX = /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/

    attr_reader :included_blaklists, :errors, :ip, :blacklists

    def initialize(ip, blacklists)
      @ip = ip
      @blacklists = blacklists
      @errors = []
    end

    def perform!
      ip.match(IP_REGEX) ? check_ip : invaid_ip!
    end

    def invaid_ip!
      (errors << 'Invalid IP specified') && []
    end

    def check_ip
      @included_blaklists = blacklists.select { |blacklist| belong_blacklist?(blacklist) }
    end

    def belong_blacklist?(blacklist)
      !IpChecker.new(ip: ip, blacklist_host: blacklist).valid
    end
  end
end
