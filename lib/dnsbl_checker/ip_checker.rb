require 'resolv'
require 'virtus'

module DnsblChecker
  class IpChecker
    include ::Virtus.model

    attribute :ip, String
    attribute :blacklist_host, String
    attribute :valid, String, default: :check_ip, lazy: true

    def check_ip
      false if Resolv.getaddress(dnsbl_host)
    rescue Resolv::ResolvError
      true
    rescue Interrupt
      false
    end

    def dnsbl_host
      "#{ip.split('.').reverse.join('.')}.#{blacklist_host}"
    end
  end
end
