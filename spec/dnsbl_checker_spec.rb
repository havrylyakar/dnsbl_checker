require 'spec_helper'

RSpec.describe 'Checker' do
  let(:array_ip) { ['51.255.91.111', '72.141.251.209'] }

  it 'should return result' do
    checker = DnsblChecker::Checker.call(array_ip)
    checker.keys.include?(array_ip)
  end
end
